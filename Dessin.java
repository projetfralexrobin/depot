import java.awt.*;
import javax.swing.*;

/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */


/**
 * class qui cr�er un plateau
 */
public class Dessin extends JPanel {
	private int nbJoueur = Jeu.getNbJoueur();
	private Joueur[] tabJ = Jeu.getTabJ();
	
	/**
	 * Methode paintComponent
	 * @param g
	 */
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int largeur = this.getWidth();
		int hauteur = this.getHeight();
		
		int j = 20;
		boolean placer = false;
		String type = "";
		
		g.setColor(Color.white);
		g.fillRect(0,0,1920,400);
		
		g.setColor(Color.black);
		g.drawLine(20,50,largeur-20,50);
		
		for (int i = 1; i <= 78; i++) {
			for (int h = 0; h < nbJoueur; h++) {
				if (tabJ[h].getRouleur().getPositionActuelleVerticale() == 0 && tabJ[h].getRouleur().getPositionActuelleHorizontale() == i) {
					placer = true;
					type = "R";
					g.setColor(setCouleur(tabJ[h].getCouleur()));
					
				}
				if(tabJ[h].getSprinteur().getPositionActuelleVerticale() == 0 && tabJ[h].getSprinteur().getPositionActuelleHorizontale() == i) {
					placer = true;
					type = "S";
					g.setColor(setCouleur(tabJ[h].getCouleur()));
				}
			}
			g.fillRect(j, 100, 15, 10);
			g.setColor(Color.black);
			if(placer) {
				g.setFont(new Font("Arial", Font.BOLD, 10));
				g.drawString(type, j+5, 110);	
			}
			placer = false;
			j = j + 20;
		}
		
		j = 20;
		for (int i = 1; i <= 78; i++) {
			for (int h = 0; h < nbJoueur; h++) {
				if (tabJ[h].getRouleur().getPositionActuelleVerticale() == 1 && tabJ[h].getRouleur().getPositionActuelleHorizontale() == i) {
					placer = true;
					type = "R";
					g.setColor(setCouleur(tabJ[h].getCouleur()));
				}	
				if (tabJ[h].getSprinteur().getPositionActuelleVerticale() == 1 && tabJ[h].getSprinteur().getPositionActuelleHorizontale() == i) {
					placer = true;
					type = "S";
					g.setColor(setCouleur(tabJ[h].getCouleur()));
				}
			}
			g.fillRect(j, 120, 15, 10);
			g.setColor(Color.black);
			if(placer) {
				g.setFont(new Font("Arial", Font.BOLD, 10)); 
				g.drawString(type, j+5, 130);	
			}
			placer = false;
			j = j + 20;
		}
		g.drawLine(20,200,largeur-20,200);	
	}
	
	/**
	 * Methode setCouleur
	 * Permet de choisir une couleur en fonction d'un string
	 * @param s
	 * @return la couleur
	 */
	
	public Color setCouleur(String s) {
		Color c = Color.black;
		if(s.equals("rouge")) {
			c = Color.red;
		}else if(s.equals("vert")) {
			c = Color.green;
		}else if(s.equals("bleu")) {
			c = Color.blue;
		}else if(s.equals("jaune")) {
			c = Color.yellow;
		}
		return c;
	}
}