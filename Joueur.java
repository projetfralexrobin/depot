/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class qui cr�er des joueurs
 */
public class Joueur {
	
	private String nomJoueur;
	private String couleur;
	private Cycliste rouleur;
	private Cycliste sprinteur;
	private PaquetEnergie paquetSprinteur;
	private PaquetEnergie paquetRouleur;
	
	/**
	 * Constructeur
	 * @param n, le nom
	 * @param c, la couleur
	 */
	
	public Joueur(String n, String c) {
		this.nomJoueur = n;
		this.couleur = c;
	}
	
	/**
	 * Methode toString 
	 * @return String, le message
	 */

	public String toString() {
		String res = "------------------------------------------\n";
		res += "Le joueur " +this.nomJoueur +" a la couleur " +this.couleur +"\n";
		res += "------------------------------------------\n";
		return res;
	}
	
	/**
	 * Mehtode getNomJoueur
	 * @return le nom du joueur
	 */
	
	public String getNomJoueur() {
		return this.nomJoueur;
	}
	
	/**
	 * Methode getCouleur
	 * @return la couleur du joueur
	 */
	
	public String getCouleur() {
		return this.couleur;
	}
	
	/**
	 * Methode setRouleur
	 * @param c
	 */
	public void setRouleur(Cycliste c) {
		if(c.getType() == 'r') {
			this.rouleur = c;
		}
	}
	
	/**
	 * Methode setSprinteur
	 * @param c
	 */
	public void setSprinteur(Cycliste c) {
		if(c.getType() == 's') {
			this.sprinteur = c;
		}
	}
	
	/**
	 * Methode getSprinteur
	 * @return spriteur
	 */
	
	public Cycliste getSprinteur() {
		return this.sprinteur;
	}
	
	/**
	 * Methode getRouleur
	 * @return rouleur
	 */
	
	public Cycliste getRouleur() {
		return this.rouleur;
	}
	
	/**
	 * Methode setPaquetEnergieSprinteur
	 * @param pe
	 */
	public void setPaquetEnergieSprinteur(PaquetEnergie pe) {
		this.paquetSprinteur = pe;
	}
	
	/**
	 * Methode setPaquetEnergieRouleur
	 * @param pe
	 */
	public void setPaquetEnergieRouleur(PaquetEnergie pe) {
		this.paquetRouleur = pe;
	}
	
	/**
	 * Methode getPaquetEnergieSprinteur
	 * @return paquetEnergie
	 */
	
	public PaquetEnergie getPaquetEnergieSprinteur() {
		return this.paquetSprinteur;
	}
	
	/**
	 * Methode getPaquetEnergieRouleur
	 * @return paquetEnergie
	 */
	
	public PaquetEnergie getPaquetEnergieRouleur() {
		return this.paquetRouleur;
	}
}

