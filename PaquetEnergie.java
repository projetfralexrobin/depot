import java.util.*;
/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class qui cr�er les paquets d'�nergie
 */
public class PaquetEnergie {
	private ArrayList<Carte> tab;
	
	/**
	 * Constructeur
	 * @param c un cycliste
	 */
	public PaquetEnergie(Cycliste c) {
		this.tab = new ArrayList<Carte>();
		CarteEnergie a1 = new CarteEnergie(2);
		CarteEnergie b1 = new CarteEnergie(3);
		CarteEnergie c1 = new CarteEnergie(4);
		CarteEnergie d1 = new CarteEnergie(5);
		CarteEnergie e1 = new CarteEnergie(6);
		CarteEnergie f1 = new CarteEnergie(7);
		CarteEnergie g1 = new CarteEnergie(9);
		
		if(c.getType() == 'r') {//3 a 7
			for(int i = 0;i<3;i++) 
				this.tab.add(b1);
			for(int i = 0;i<3;i++) 
				this.tab.add(c1);
			for(int i = 0;i<3;i++) 
				this.tab.add(d1);
			for(int i = 0;i<3;i++) 
				this.tab.add(e1);
			for(int i = 0;i<3;i++) 
				this.tab.add(f1);
		}
		if(c.getType() == 's') {//2 a 5 et 9
			for(int i = 0;i<3;i++) 
				this.tab.add(a1);
			for(int i = 0;i<3;i++) 
				this.tab.add(b1);
			for(int i = 0;i<3;i++) 
				this.tab.add(c1);
			for(int i = 0;i<3;i++) 
				this.tab.add(d1);
			for(int i = 0;i<3;i++) 
				this.tab.add(g1);
			
		}
		Collections.shuffle(this.tab);
	}
	
	/**
	 * Methode getTab
	 * @return la liste
	 */
	public ArrayList<Carte> getTab() {
		return tab;
	}
}
