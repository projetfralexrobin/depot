/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class qui cr�er des circuits 
 */
public class Circuit {
	
	/**
	 * 2 attributs :
	 * nomCircuit, le nom du circuit
	 * circuit, la base du circuit
	 */
	
	private String nomCircuit;
	private Tuile[] circuit;
	
	/**
	 * Constructeur
	 * @param n
	 * @throws TuileException
	 * @throws CircuitException 
	 */
	
	public Circuit(String n) throws TuileException, CircuitException {
		this.nomCircuit = n;
		this.setCircuit();
	}
	
	/**
	 * Methode setCircuit
	 * @throws TuileException
	 * @throws CircuitException 
	 */
	
	public void setCircuit() throws TuileException, CircuitException {
		circuit = new Tuile[21];
		if (this.nomCircuit.equals("Avenue Corsa Paseo")){
			
			circuit[0] = new Tuile('a',6,"LDD",0);
			circuit[1] = new Tuile('b',6,"LDD",0);
			circuit[2] = new Tuile('c',6,"LDD",0);
			circuit[3] = new Tuile('d',6,"LDD",0);
			circuit[4] = new Tuile('e',2,"VSG",0);
			circuit[5] = new Tuile('f',6,"LDD",0);
			circuit[6] = new Tuile('g',2,"VSG",0);
			circuit[7] = new Tuile('h',2,"VSG",0);
			circuit[8] = new Tuile('i',2,"VLG",0);
			circuit[9] = new Tuile('j',2,"VLD",0);
			circuit[10] = new Tuile('k',2,"VSD",0);
			circuit[11] = new Tuile('l',6,"LDN",0);
			circuit[12] = new Tuile('m',6,"LDN",0);
			circuit[13] = new Tuile('n',6,"LDN",0);
			circuit[14] = new Tuile('o',2,"VSD",0);
			circuit[15] = new Tuile('p',2,"VSD",0);
			circuit[16] = new Tuile('q',2,"VSG",0);
			circuit[17] = new Tuile('r',2,"VSD",0);
			circuit[18] = new Tuile('s',2,"VSD",0);
			circuit[19] = new Tuile('t',2,"VSG",0);
			circuit[20] = new Tuile('u',6,"LDD",0);
		}else{
			throw new CircuitException("Circuit non trouve...");
		}
	}
	
	/**
	 * Methode toString
	 * @return un String contenant le message du circuit
	 */
	
	public String toString(){
		String res = "";
		if (this.circuit != null) {
			res = "Parcours du circuit : \n";
			for (int i = 0; i < 21; i++) {
				res += circuit[i].getType() +", ";
			}
		}
		return res;
	}
	
	/**
	 * Methode tuileALaPosition
	 * @param n
	 * @return la tuile a la position n
	 */
	
	public Tuile tuileALaPosition(int n) {
		return circuit[n];
	}
	
	/**
	 * Methode positionEnDescente
	 * @return boolean, vrai si la position est en descente
	 */
	

	public boolean positionEnDescente(Cycliste c) {
		boolean res = false;
		if (circuit[c.getPositionActuelleHorizontale()].getPente() == -1)
			res = true;
		
		return res;
	}	
	
	/**
	 * Methode positionNormale
	 * @return boolean, vrai si la position est normale
	 */
	
	public boolean positionNormale(Cycliste c) {
			boolean res = false;
			if (circuit[c.getPositionActuelleHorizontale()].getPente() == 0)
				res = true;
			
			return res;
	}
	
	/**
	 * Methode positionEnMontee
	 * @return boolean, vrai si la position est en montee
	 */
	
	public boolean positionEnMontee(Cycliste c) {
			boolean res = false;
			if (circuit[c.getPositionActuelleHorizontale()].getPente() == 1)
				res = true;
			
			return res;
	}
	
	/**
	 * Methode getNomCircuit
	 * @return le nom du circuit
	 */
	
	public String getNomCircuit() {
		return this.nomCircuit;
	}
	
	/**
	 * Methode getCircuit
	 * @return circuit
	 */
	
	public Tuile[] getCircuit() {
		return this.circuit;
	}
	
	/**
	 * Methode getTuile
	 * @param n
	 * @return la tuile du circuit a la position n
	 */
	
	public Tuile getTuile(int n) {
		return this.circuit[n];
	}
	
	/**
	 * Methode getNbCases
	 * @return le nombre de cases du circuit
	 */
	
	public int getNbCases() {
		int res = 0;
		for (int i = 0; i < circuit.length; i++) {
			res += circuit[i].getNombreDeCases();
		}
		return res;
	}
}


