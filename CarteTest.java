import static org.junit.Assert.*;

import org.junit.Test;

/**
 * test de carte
 */
public class CarteTest {

	@Test
	public void testCarte() {
		//Pr�paration des donn�es
		Carte c = new Carte(6);
		//Test
		assertEquals("Le numero devrait etre 6",c.getNum(),6);
	}
	
	@Test
	public void testCarteEnergie() {
		//Pr�paration des donn�es
		Carte c = new CarteEnergie(6);
		//Test
		assertEquals("Le numero devrait etre 6",c.getNum(),6);
	}
	
	@Test
	public void testCarteFatique() {
		//Pr�paration des donn�es
		Carte c = new CarteFatigue();
		//Test
		assertEquals("Le numero devrait etre 2",c.getNum(),2);
	}
}
