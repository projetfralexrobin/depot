/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * exception
 */
public class PositionPionException extends Exception{
	/**
	 * Constructeur
	 * @param s, le message envoye
	 */
	public PositionPionException(String s) {
		super(s);
	}
}
