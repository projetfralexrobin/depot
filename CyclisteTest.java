import static org.junit.Assert.*;

import org.junit.Test;

/**
 * class qui test les methodes cyclistes
 */
public class CyclisteTest {

	@Test
	public void testConstructeurCycliste() {
		//Pr�paration des donn�es
		Cycliste c = new Cycliste('s',5,1);
		//test 
		assertEquals("Le cycliste devrait etre un sprinteur",c.getType(),'s');
		assertEquals("Le cycliste devrait etre a la position 5",c.getPositionActuelleHorizontale(),5);
		assertEquals("Le cycliste devrait etre a la position 1",c.getPositionActuelleVerticale(),1);
	}
	
	@Test
	public void testSetPositionActuelleVerticale() {
		//Pr�paration des donn�es
		Cycliste c = new Cycliste('s',5,1);
		c.setPositionActuelleVerticale(0);
		//test 
		assertEquals("Le cycliste devrait etre a la position 0",c.getPositionActuelleVerticale(),0);
	}
	
	@Test
	public void testSetPositionActuelleHorizontale() {
		//Pr�paration des donn�es
		Cycliste c = new Cycliste('s',5,1);
		c.setPositionActuelleHorizontale(3);
		//test 
		assertEquals("Le cycliste devrait etre a la position 3",c.getPositionActuelleHorizontale(),3);
	}
}
