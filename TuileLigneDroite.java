/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * cr�er des tuiles ligne droite herite de tuile
 */
public class TuileLigneDroite extends Tuile{
	
	private String type;
	
	/**
	 * Constructeur
	 * @param c
	 * @param t
	 * @param p
	 * @throws TuileException
	 */
	
	public TuileLigneDroite(char c,String t, int p) throws TuileException {
		super(c,6,t,p);
		this.type = t;
	}
	
	/**
	 * Methode estLigneDepart
	 * @return boolean, vrai si la tuile est sur la meme ligne que celle du depart
	 */
	
	public boolean estLigneDepart() {
		boolean res = false;
		if (this.type.equals("LDD")) {
			res = true;
		}
		return res;
	}
	
	/**
	 * Methode estLigneArrivee
	 * @return boolean, vrai si la tuile est sur la meme ligne que celle de l'arrivee
	 */

	public boolean estLigneArrivee(){
		boolean res = false;
		if (this.type.equals("LDA")) {
			res = true;
		}
		return res;
	}
	

	/**
	 * Methode estLigneNormale
	 * @return boolean, vrai si la tuile est une ligne normale
	 */

	public boolean estLigneNormale(){
		boolean res = false;
		if (this.type.equals("LDN")) {
			res = true;
		}
		return res;
	}
}
