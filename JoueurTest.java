import static org.junit.Assert.*;

import org.junit.Test;


/**
 *test pour joueur
 */
public class JoueurTest {

	@Test
	public void testConstructeurJoueur() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		//test
		assertEquals("Le nom du joueur devrait etre Robin",j.getNomJoueur(),"Robin");
		assertEquals("La couleur du joueur devraut etre rouge",j.getCouleur(),"rouge");
	}
	
	@Test
	public void testSetRouleur() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		Cycliste c = new Cycliste('r',5,1);
		//test
		j.setRouleur(c);
		assertEquals("Le cycliste aurait du etre ajout�",j.getRouleur(),c);
	}
	
	@Test
	public void testSetRouleur2() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		Cycliste c = new Cycliste('s',5,1);
		//test
		j.setRouleur(c);
		assertEquals("Le cycliste n'aurait pas du etre ajout�",j.getRouleur(),null);
	}
	
	@Test
	public void testSetSprinteur() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		Cycliste c = new Cycliste('s',5,1);
		//test
		j.setSprinteur(c);
		assertEquals("Le cycliste aurait du etre ajout�",j.getSprinteur(),c);
	}
	
	@Test
	public void testSetSprinteur2() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		Cycliste c = new Cycliste('r',5,1);
		//test
		j.setSprinteur(c);
		assertEquals("Le cycliste n'aurait pas du etre ajout�",j.getSprinteur(),null);
	}
	
	@Test
	public void tesSetPaquetEnergieSprinteur() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		Cycliste c = new Cycliste('r',5,1);
		PaquetEnergie e = new PaquetEnergie(c);
		//test
		j.setPaquetEnergieSprinteur(e);
		assertEquals("Le paquet aurait du etre ajout�",j.getPaquetEnergieSprinteur(),e);
	}
	
	@Test
	public void tesSetPaquetEnergieRouleur() {
		//Preparation des donn�es
		Joueur j = new Joueur("Robin","rouge");
		Cycliste c = new Cycliste('r',5,1);
		PaquetEnergie e = new PaquetEnergie(c);
		//test
		j.setPaquetEnergieRouleur(e);
		assertEquals("Le paquet aurait du etre ajout�",j.getPaquetEnergieRouleur(),e);
	}

}
