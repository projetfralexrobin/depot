/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class d'exception
 */
public class TuileException extends Exception{
	
	/**
	 * Constructeur
	 * @param s, le message envoye
	 */
	public TuileException(String s) {
		super(s);
	}
}
