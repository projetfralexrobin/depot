/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * cr�er des tuiles virages herite de tuile
 */
public class TuileVirage extends Tuile{
	
	private String type;
	
	/**
	 * Construsteur
	 * @param c
	 * @param nb
	 * @param t
	 * @param p
	 * @throws TuileException
	 */
	
	public TuileVirage(char c, String t, int p) throws TuileException {
		super(c,2,t,p);
		this.type = t;
	}
	
	/**
	 * Methode estLeger
	 * @return boolean, vrai si le virage est leg�
	 */
	
	public boolean estLeger() {
		boolean res = false;
		if (this.type.equals("VLG") || this.type.equals("VLD")) {
			res = true;
		}
		return res;
	}
	
	/**
	 * Methode estSerre
	 * @return boolean, vrai si le virage est serre
	 */
	
	public boolean estSerre() {
		boolean res = false;
		if (this.type.equals("VSG") || this.type.equals("VSD")) {
			res = true;
		}
		return res;
	}
}
