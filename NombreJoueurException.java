/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * exception
 */
public class NombreJoueurException extends Exception{
	/**
	 * Constructeur
	 * @param s, le message envoye
	 */
	public NombreJoueurException(String s) {
		super(s);
	}
}
