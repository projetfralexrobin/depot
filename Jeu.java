import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;


/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class "moteur" du jeu
 */
public class Jeu {
	
	private static String phrase;
	private static int nbJoueur;
	private Circuit circ = new Circuit("Avenue Corsa Paseo");
	private static boolean fin;
	private static Joueur[] tabJ;
	private static Cycliste c1;
	private static Cycliste c2;
	private static String[][] plateau;
	private static int nbCases;
	private static Cycliste[] classement;
	
	/**
	 * Constructeur
	 * @throws TuileException
	 * @throws CircuitException
	 * @throws NombreJoueurException
	 * @throws PositionPionException
	 * @throws IOException
	 */
	
	public Jeu() throws TuileException, CircuitException, NombreJoueurException, PositionPionException, IOException{
		this.phrase = "D�but du jeu !";
		this.fin = false;
		this.demarrerJeu();
		this.positionDepart();
		this.afficherJeu();
		
	}
	
	/**
	 * Methode demarrerJeu
	 * Demarre le jeu
	 * @throws TuileException
	 * @throws CircuitException
	 * @throws NombreJoueurException
	 * @throws PositionPionException
	 * @throws IOException
	 */
	
	public void demarrerJeu() throws TuileException, CircuitException, NombreJoueurException, PositionPionException, IOException{
		//afficherPlateau();
		
		System.out.println(this.phrase);
		Scanner sc = new Scanner(System.in);
		//On demande le nombre de joueurs
		System.out.print("Entrez le nombre de joueurs : \n");
	    nbJoueur = sc.nextInt();
	    //Ce nombre de etre compris entre 2 et 4 conform�ment aux regles du jeu
	    if (this.nbJoueur >= 2 && this.nbJoueur <= 4) {
	    	tabJ = new Joueur[nbJoueur];
	    }else {
	    	throw new PositionPionException("Le nombre de joueurs ne correspond pas au jeu");
	    }
	    //On cr�e les couleurs qui vont etre attribu�es, le noir est remplac� par le jaune pour cause de soucis technique
		String[] couleurs = {"rouge","vert","bleu","jaune"};
		//On cr�e le circuit
		circ = new Circuit("Avenue Corsa Paseo");
		
		//On demande a chaque joueur son nom et on lui attribut sa couleur
		for (int i = 0; i < this.nbJoueur; i++){
			System.out.print("\nEntrez le nom du joueur " +i +" : \n");
			String nom = sc.next();
			tabJ[i] = new Joueur(nom,couleurs[i]); 
			System.out.println(tabJ[i]);
		}
		//On efface toutes donn�es du fichier qui recoit le gagnant et on l'initialise
		new FileWriter(new File("Fichier.txt")).close(); 
		
		 this.phrase = "\nD�but de la partie !\n";
		 System.out.println(this.phrase); 
	}
	
	/**
	 * Methode positionDepart
	 * Place les diff�rents cycliste sur le plateau
	 * @throws PositionPionException
	 * @throws NombreJoueurException
	 */
	
	public void positionDepart() throws PositionPionException, NombreJoueurException{
		Scanner sc = new Scanner(System.in);
		//On prend le nombre de cases en longueur du circuit
		nbCases = circ.getNbCases();
		//On cr�e un tableau de string qui va recevoir la position des pions
		plateau = new String[2][nbCases];
		//afficherConsole();
				
		for (int i = 0; i < this.nbJoueur; i++){
			/**
			 * On demande au joueur la position de son rouleur
			 */
			System.out.println(tabJ[i].getNomJoueur() +" : Entrez la position de votre rouleur (position de 0 a 5) : "); //position horizontale
			int positionH = sc.nextInt();
			if (positionH < 0 || positionH > 5) {
				throw new PositionPionException("Impossible de placer le pion a cet endroit...");
			}
			
			System.out.println(tabJ[i].getNomJoueur() +" : Entrez la position de votre rouleur (position de 0 a 1) : "); //position verticale
			int positionV = sc.nextInt();
			if (positionV < 0 || positionV > 1) {
				throw new PositionPionException("Impossible de placer le pion a cet endroit...");
			}
			/*
			 * On cr�e un cycliste de type rouleur
			 * On regarde si la position choisie n'est pas d�j� attribu�e
			 */
					
			c1 = new Cycliste('r',positionH,positionV); // Rouleur 
			if(plateau[positionV][positionH] == null)
				plateau[positionV][positionH] = tabJ[i].getNomJoueur() + " : rouleur";
			else
				throw new PositionPionException("Cet emplacement est deja attribu�...");
			
			//On ajoute au joueur j le rouleur
			tabJ[i].setRouleur(c1);
					
			/**
			* On demande au joueur la position de son sprinteur
		    */
			System.out.println(tabJ[i].getNomJoueur() +" : Entrez la position de votre sprinteur (position de 0 a 5) : ");//position horizontale
		    positionH = sc.nextInt();
		    if (positionH < 0 || positionH > 5) {
				throw new PositionPionException("Impossible de placer le pion a cet endroit...");
			}
		    
			System.out.println(tabJ[i].getNomJoueur() +" : Entrez la position de votre sprinteur (position de 0 a 1) : "); //position verticale
			positionV = sc.nextInt();
			if (positionV < 0 || positionV > 1) {
				throw new PositionPionException("Impossible de placer le pion a cet endroit...");
			}
				    
			/**
			 * On cr�e un cycliste de type sprinteur
			 * on regarde si la position choisie n'est pas d�j� attribu�e
			 */
					
			c2 = new Cycliste('s',positionH,positionV); // Sprinteur
			if(plateau[positionV][positionH] == null && positionH >= 0 && positionH <= 5 && positionV >= 0 && positionV <= 1)
				plateau[positionV][positionH] = tabJ[i].getNomJoueur() + " : sprinteur";
			else
				throw new NombreJoueurException("Cette emplacements est deja attribu�...");
			
			//On ajoute au joueur j le sprinteur
			tabJ[i].setSprinteur(c2);
		}
	}
	
	/**
	 * Methode estFini
	 * @return vrai si un cycliste a fini
	 * @throws IOException
	 */
	
	public static boolean estFini() throws IOException{
		for (int i = 0; i < nbJoueur; i++) {
			if (tabJ[i].getRouleur().getPositionActuelleHorizontale() >= nbCases-5 || tabJ[i].getSprinteur().getPositionActuelleHorizontale() >= nbCases-5) {
				BufferedWriter fichier = new BufferedWriter(new FileWriter("Fichier.txt"));
				fichier.write("Le gagnant est : " +tabJ[i].getNomJoueur());
				System.out.println("Le gagnant est : " +tabJ[i].getNomJoueur());
				fin = true;
				fichier.close();
			}
		}
		return fin;
	}
	
	/**
	 * Methode avancerPion
	 * Permet d'avancer le cycliste c du joueur j de n cases
	 * @param n
	 * @param c
	 * @param j
	 */
	
	public static void avancerPion(int n, Cycliste c, Joueur j) {
		boolean fini = false;
		int posPrecH = c.getPositionActuelleHorizontale();
		int posPrecV = c.getPositionActuelleVerticale();
		int posAct = posPrecH + n;
		int posVerticale = c.getPositionActuelleVerticale();
		int numero = n;
		String type = "";
		
		if(c.getType() == 'r') {
			type = "rouleur";
			while(!fini) {
				if(plateau[j.getRouleur().getPositionActuelleVerticale()][j.getRouleur().getPositionActuelleHorizontale() + numero] == null) {
					fini = true;
				}else if(j.getRouleur().getPositionActuelleVerticale() == 0) {
					if (plateau[j.getRouleur().getPositionActuelleVerticale() + 1][j.getRouleur().getPositionActuelleHorizontale() + numero] == null) {
						posVerticale++;
						fini = true;
					}else {
						numero--;
						posAct--;
					}
				}else if ((j.getRouleur().getPositionActuelleVerticale() == 1)) {
					if (plateau[j.getRouleur().getPositionActuelleVerticale() - 1][j.getRouleur().getPositionActuelleHorizontale() + numero] == null) {
						posVerticale--;
						fini = true;
					}else {
						numero--;
						posAct--;
					}
				}
			}
		}else if (c.getType() == 's') {
			type = "sprinteur";
			while(!fini) {
				if(plateau[j.getSprinteur().getPositionActuelleVerticale()][j.getSprinteur().getPositionActuelleHorizontale() + numero] == null) {
					fini = true;
				}else if(j.getSprinteur().getPositionActuelleVerticale() == 0) {
					if (plateau[j.getSprinteur().getPositionActuelleVerticale() + 1][j.getSprinteur().getPositionActuelleHorizontale() + numero] == null) {
						posVerticale++;
						fini = true;	
					}else{
						numero--;
						posAct--;
					}
				}else if ((j.getSprinteur().getPositionActuelleVerticale() == 1)) {
					if (plateau[j.getSprinteur().getPositionActuelleVerticale() -1][j.getSprinteur().getPositionActuelleHorizontale() + numero] == null) {
						posVerticale--;
						fini = true;
					}else {
						numero--;
						posAct--;
					}
				}
			}
		}
		
		c.setPositionActuelleHorizontale(posAct);
		c.setPositionActuelleVerticale(posVerticale);
		plateau[posVerticale][posAct] = j.getNomJoueur() +" : " +type;
		plateau[posPrecV][posPrecH] = null;
		//afficherConsole();
	}
	
	/**
	 * Methode avancerToutDroit
	 * Permet d'avancer le cycliste c du joueur j de n cases sans changer de file
	 * @param n
	 * @param c
	 * @param j
	 */
	
	public static void avancerToutDroit(Cycliste c, Joueur j) {
		int posPrecH = c.getPositionActuelleHorizontale();
		int posPrecV = c.getPositionActuelleVerticale();
		int posAct = posPrecH + 1;
		String type = "";
		
		if (c.getType() == 'r') {
			if(plateau[j.getRouleur().getPositionActuelleVerticale()][j.getRouleur().getPositionActuelleHorizontale() + 1] == null) {
				type = "rouleur";
				c.setPositionActuelleHorizontale(posAct);
				plateau[posPrecV][posAct] = j.getNomJoueur() +" : " +type;
				plateau[posPrecV][posPrecH] = null;
			}
		}else if (c.getType() == 's') {
			if(plateau[j.getSprinteur().getPositionActuelleVerticale()][j.getSprinteur().getPositionActuelleHorizontale() + 1] == null) {
				type = "sprinteur";
				c.setPositionActuelleHorizontale(posAct);
				plateau[posPrecV][posAct] = j.getNomJoueur() +" : " +type;
				plateau[posPrecV][posPrecH] = null;
			}
		}
	}
	
	/**
	 * Methode changerFile
	 * Permet de savoir si le joueur j peut changer de file (de la file gauche vers la droite) avec son cycliste c
	 * @param j
	 * @param c
	 * @return boolean, vrai si il peut changer
	 */
	
	public static boolean changerFile(Joueur j, Cycliste c) {
		boolean res = false;

		if (c.getType() == 's') {
			if ((j.getSprinteur().getPositionActuelleVerticale() == 0)) {
				if (plateau[j.getSprinteur().getPositionActuelleVerticale() +1][j.getSprinteur().getPositionActuelleHorizontale()] == null) {
					res = true;
				}	
			}
		}else if (c.getType() == 'r') {
			if ((j.getRouleur().getPositionActuelleVerticale() == 0)) {
				if (plateau[j.getRouleur().getPositionActuelleVerticale() +1][j.getRouleur().getPositionActuelleHorizontale()] == null) {
					res = true;
				}	
			}
		}
		return res;
	}
	
	/**
	 * Methode changerPositionFile
	 * Change le cycliste c du joueur j de file
	 * @param j
	 * @param c
	 */
	
	public static void changerPositionFile(Joueur j, Cycliste c) {
		String type = "";
		int posHorizontale = c.getPositionActuelleHorizontale();
		int posVerticale = c.getPositionActuelleVerticale();
		int posPrecVerticale = c.getPositionActuelleVerticale();;
		
		if(c.getType() == 'r') {
			type = "rouleur";
		}else if(c.getType() == 's'){
			type = "sprinteur";
		}
		
		if (posVerticale == 0) {
			plateau[posVerticale+1][posHorizontale] = j.getNomJoueur() +" : " +type;
			c.setPositionActuelleVerticale(posVerticale+1);
		}
		plateau[posPrecVerticale][posHorizontale] = null;
	}
	
	/**
	 * Methode afficherJeu
	 * Affiche le status du jeu
	 */

	public static void afficherJeu() {
		for (int i = 0; i < nbCases; i++) {
			for (int j = 0; j < 2; j++) {
				if (plateau[j][i] != null){
					System.out.println("Case "+j +":" +i +" = " +plateau[j][i]);
				}
			}
		}
		System.out.println("\n");
	}
	/**
	 * Methode afficherPlateau
	 * affiche le plateau utilis� dans le fichier texte 
	 */
	
	public void afficherPlateau() {
		char tabaffiche[][] = new char[12][28];
		FileReader fr;
		try {
			fr = new FileReader("plateau.txt");
			int i = 0;
			int j = 0;
			int c;
			while((c = fr.read()) != -1) {
				if(c == '\n') {
					j = 0;
					i++;
				} else if(c != '\r') {
					tabaffiche[i][j] = (char) c;
					j++;
				}
			}
			fr.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < 12; i++) {
			for (int j = 0; j < 28; j++) {
				System.out.print(tabaffiche[i][j]);
			}
			System.out.print("\n");
		}
	}
	
	/**
	 * Methode estAspirer
	 * Fait l'aspiration cycliste par cycliste
	 */
	
	public static void estAspirer() {
		int posh;
		int posv;
		int caseCote = 1;
		boolean fini = false;
		classement = classementCycliste();
		
		for(int w =0;w<15;w++) { // max 15
			for(int i = 0;i<classement.length;i++) {
				posh = classement[i].getPositionActuelleHorizontale();
				posv = classement[i].getPositionActuelleVerticale();
				if(plateau[posv][posh+2] != null && plateau[posv][posh+1] == null){
					for(int z = 0;z<tabJ.length;z++) {
						if(tabJ[z].getRouleur().getPositionActuelleHorizontale() == classement[i].getPositionActuelleHorizontale() && tabJ[z].getRouleur().getPositionActuelleVerticale() == classement[i].getPositionActuelleVerticale()) {
							avancerToutDroit(classement[i],tabJ[z]);
						}
						else if(tabJ[z].getSprinteur().getPositionActuelleHorizontale() == classement[i].getPositionActuelleHorizontale() && tabJ[z].getSprinteur().getPositionActuelleVerticale() == classement[i].getPositionActuelleVerticale()) {
							avancerToutDroit(classement[i],tabJ[z]);
						}
					}
				}
			}
		}
		for(int i = 0;i<classement.length;i++) {
			posh = classement[i].getPositionActuelleHorizontale();
			posv = classement[i].getPositionActuelleVerticale();
			if(plateau[posv][posh+1] == null)
				for(int z = 0;z<tabJ.length;z++) {
					if(tabJ[z].getRouleur().getPositionActuelleHorizontale() == classement[i].getPositionActuelleHorizontale() && tabJ[z].getRouleur().getPositionActuelleVerticale() == classement[i].getPositionActuelleVerticale()) {
						donnerFatigue(classement[i],tabJ[z]);
					}
					else if(tabJ[z].getSprinteur().getPositionActuelleHorizontale() == classement[i].getPositionActuelleHorizontale() && tabJ[z].getSprinteur().getPositionActuelleVerticale() == classement[i].getPositionActuelleVerticale()) {
						donnerFatigue(classement[i],tabJ[z]);
					}
				}
		}
			
	}
	
	/**
	 * Methode donnerFatigue
	 * Ajoute une carte fatigue au cycliste c du joueur j
	 * @param c
	 * @param j
	 */
	
	public static void donnerFatigue(Cycliste c,Joueur j) {
			if(c.getType()=='r') {
				j.getPaquetEnergieRouleur().getTab().add(j.getPaquetEnergieRouleur().getTab().size(), new CarteFatigue());;
			}else {
				j.getPaquetEnergieSprinteur().getTab().add(j.getPaquetEnergieSprinteur().getTab().size(), new CarteFatigue());;
			}
		
	}
	
	/**
	 * M�thode classementCycliste
	 * Permet de donner le classement de cycliste tri� du plus avanc� au moins avanc�
	 * @return le tableau de cycliste tri�
	 */

	public static Cycliste[] classementCycliste() {
		Cycliste[] classement = new Cycliste[2*nbJoueur];
		int indic = 0;
		for(int i = 0; i < 78; i++) {
			for(int j = 0; j < 2; j++) {
				for (int h = 0; h < nbJoueur; h++) {
					if (tabJ[h].getRouleur().getPositionActuelleHorizontale() == i && tabJ[h].getRouleur().getPositionActuelleVerticale() == j) {
						classement[indic] = tabJ[h].getRouleur();
						indic++;
					}else if (tabJ[h].getSprinteur().getPositionActuelleHorizontale() == i && tabJ[h].getSprinteur().getPositionActuelleVerticale() == j) {
						classement[indic] = tabJ[h].getSprinteur();
						indic++;
					}
				}
			}
		}
		return classement;
	}
	
	/**
	 * Methode ajouterCarte
	 * Permet d'ajouter un carte fatigue si le joueur n'en a plus
	 * @param pe
	 */
	
	public static void ajouterCarte(PaquetEnergie pe) {
		CarteFatigue e = new CarteFatigue();
		if(pe.getTab().size()<= 4) {
			pe.getTab().add(e);
		}
	}
	
	/**
	 * Methode getNbJoueur
	 * @return le nombre de joueur
	 */
	
	public static int getNbJoueur() {
		return nbJoueur;
	}
	
	/**
	 * Methode getTabJ
	 * @return le tableau de joueur
	 */
	
	public static Joueur[] getTabJ() {
		return tabJ;
	}
	
	/**
	 * main
	 * @param args
	 * @throws TuileException
	 * @throws CircuitException
	 * @throws NombreJoueurException
	 * @throws PositionPionException
	 * @throws IOException
	 */
	public static void main(String[] args) throws TuileException, CircuitException, NombreJoueurException, PositionPionException, IOException {
		Jeu jeu = new Jeu();
		Scanner sc = new Scanner(System.in);
		
		//Creation de la JFrame qui va acceuillir le dessin du plateau
		JFrame fen = new JFrame("Dessin");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dessin dess = new Dessin();
		dess.setPreferredSize(new Dimension(1920,400));
		fen.setContentPane(dess);
		fen.pack();
		fen.setVisible(true);
		
		//Creation des paquets
		PaquetFatigue paquetFatigueSprinteur = new PaquetFatigue();
		PaquetFatigue paquetFatigueCourreur = new PaquetFatigue();
		
		if (nbJoueur >= 2){
			PaquetEnergie paquetJ1S = new PaquetEnergie(tabJ[0].getSprinteur());
			PaquetEnergie paquetJ1R = new PaquetEnergie(tabJ[0].getRouleur());
			tabJ[0].setPaquetEnergieSprinteur(paquetJ1S);
			tabJ[0].setPaquetEnergieRouleur(paquetJ1R);
			PaquetEnergie paquetJ2S = new PaquetEnergie(tabJ[1].getSprinteur());
			PaquetEnergie paquetJ2R = new PaquetEnergie(tabJ[1].getRouleur());
			tabJ[1].setPaquetEnergieSprinteur(paquetJ2S);
			tabJ[1].setPaquetEnergieRouleur(paquetJ2R);
		}
		if (nbJoueur >= 3){
			PaquetEnergie paquetJ3S = new PaquetEnergie(tabJ[2].getSprinteur());
			PaquetEnergie paquetJ3R = new PaquetEnergie(tabJ[2].getRouleur());
			tabJ[2].setPaquetEnergieSprinteur(paquetJ3S);
			tabJ[2].setPaquetEnergieRouleur(paquetJ3R);
		}
		if (nbJoueur == 4){
			PaquetEnergie paquetJ4S = new PaquetEnergie(tabJ[3].getSprinteur());
			PaquetEnergie paquetJ4R = new PaquetEnergie(tabJ[3].getRouleur());
			tabJ[3].setPaquetEnergieSprinteur(paquetJ4S);
			tabJ[3].setPaquetEnergieRouleur(paquetJ4R);
		}
		int i;
		Carte c;
		while (!estFini()){
			for(i = 0; i < nbJoueur; i++){
				if (!estFini()) {
					//On demande au joueur quel cycliste il veut avancer
					System.out.println(tabJ[i].getNomJoueur() +" : quel cycliste voulez-vous avancer ? (rouleur/sprinteur)");
					String rep = sc.next();
					//Si la r�ponse n'est pas bonne, on lui redemande
					while (!rep.equals("rouleur") && !rep.equals("sprinteur")){
						System.out.println("Erreur, veuillez reessayer : (rouleur/sprinteur)");
						rep = sc.next();
					}
					//On le fait piocher et choisir
					if (rep.equals("rouleur")){
						c = tabJ[i].getRouleur().piocher4carte(tabJ[i].getPaquetEnergieRouleur().getTab());
						avancerPion(c.getNum(),tabJ[i].getRouleur(),tabJ[i]);
						dess.repaint();
						if(changerFile(tabJ[i],tabJ[i].getRouleur())) {
							changerPositionFile(tabJ[i],tabJ[i].getRouleur());
							
						}
						afficherJeu();
						dess.repaint();
						//On lui fait avancer son sprinteur car tout les pions doivent etre d�plac�s pendant le tour
						System.out.println("\nVous devez maintenant avancer votre sprinteur");
						c = tabJ[i].getSprinteur().piocher4carte(tabJ[i].getPaquetEnergieSprinteur().getTab());
						avancerPion(c.getNum(),tabJ[i].getSprinteur(),tabJ[i]);
						dess.repaint();
						if(changerFile(tabJ[i],tabJ[i].getSprinteur())) {
							changerPositionFile(tabJ[i],tabJ[i].getSprinteur());
						}
						
						afficherJeu();
					}else if (rep.equals("sprinteur")){
						c = tabJ[i].getSprinteur().piocher4carte(tabJ[i].getPaquetEnergieSprinteur().getTab());
						avancerPion(c.getNum(),tabJ[i].getSprinteur(),tabJ[i]);
						dess.repaint();
						if(changerFile(tabJ[i],tabJ[i].getSprinteur())) {
							changerPositionFile(tabJ[i],tabJ[i].getSprinteur());
						}
						afficherJeu();
						dess.repaint();
						//On lui fait avancer son sprinteur car tout les pions doivent etre d�plac�s pendant le tour
						System.out.println("\nVous devez maintenant avancer votre rouleur");
						c = tabJ[i].getRouleur().piocher4carte(tabJ[i].getPaquetEnergieRouleur().getTab());
						avancerPion(c.getNum(),tabJ[i].getRouleur(),tabJ[i]);
						dess.repaint();
						if(changerFile(tabJ[i],tabJ[i].getRouleur())) {
							changerPositionFile(tabJ[i],tabJ[i].getRouleur());
						}
					}
					//On ajoute des cartes si on n'a plus assez de cartes
					ajouterCarte(tabJ[i].getPaquetEnergieRouleur());
					ajouterCarte(tabJ[i].getPaquetEnergieSprinteur());
					//melange
					Collections.shuffle(tabJ[i].getPaquetEnergieRouleur().getTab());
					Collections.shuffle(tabJ[i].getPaquetEnergieSprinteur().getTab());
					
					afficherJeu();
					dess.repaint();
				}
			}
			classementCycliste();
			estAspirer();
		}
		//On ferme la fenetre 
		fen.setVisible(false);
		fen.dispose();
		
		phrase = "Partie terminee ! ";
		System.out.println(phrase);
	}	
}
