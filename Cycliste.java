import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class qui cr�er des cyclistes
 */
public class Cycliste {
	private char type;
	private int positionActuelleH;
	private int positionActuelleV;
	
	/**
	 * Constructeur
	 * @param t le type
	 * @param pos la position du cycliste
	 * @param jou a quelle joueur il appartient
	 */
	public Cycliste(char t,int posH, int posV) {
		this.type = t;
		this.positionActuelleH = posH;
		this.positionActuelleV = posV;
	}
	
	/**
	 * Methode getType
	 * @return le type
	 */
	public char getType() {
		return type;
	}
	
	/**
	 * Methode getPositionActuelleHorizontale
	 * @return la position actuelle horizontale
	 */
	public int getPositionActuelleHorizontale() {
		return positionActuelleH;
	}
	
	/**
	 * Methode getPositionActuelleVerticale
	 * @return la position actuelle verticale
	 */
	public int getPositionActuelleVerticale() {
		return positionActuelleV;
	}
	
	/**
	 * Methode setPositionActuelleHorizontale
	 * @param p 
	 */
	
	public void setPositionActuelleHorizontale(int p) {
		 this.positionActuelleH = p;
	}
	
	/**
	 * Methode setPositionActuelleVerticale
	 * @param p
	 */
	
	public void setPositionActuelleVerticale(int p) {
		this.positionActuelleV = p;
	}
	
	/**
	 * Methode toString
	 * @return String, message
	 */
	
	
	public String toString() {
		String typeC;
		if (this.type == ('s')) {
			typeC = "Sprinteur";
		}else if (this.type == ('r')){
			typeC = "Rouleur";
		}else {
			typeC = null;
		}
		
		String res = "Le cycliste du joueur est un " +typeC;
		return res;
	}
	
	/**
	 * Methode piocher4carte
	 * Permet au joueur de piocher 4 carte puis d'en choisir une 
	 * @param tab
	 * @return la carte choisie
	 */
	
	
	public Carte piocher4carte(ArrayList<Carte> tab1){
		Scanner sc = new Scanner(System.in);
		for(int i = 0;i<4;i++) {
			System.out.println(i+1 +")" +tab1.get(i).getNum());
		}
		System.out.println("Choisi ta carte (1, 2, 3 ou 4) : ");
		
		int rep = sc.nextInt();
		Carte res = null;
		
		switch(rep) {
		case 1 :
			res = tab1.get(0);
			
			tab1.add(tab1.size(), tab1.get(1));
			tab1.add(tab1.size(), tab1.get(2));
			tab1.add(tab1.size(), tab1.get(3));
			for(int i =0;i<4;i++)
				tab1.remove(0);
			break;
		case 2 : 
			res = tab1.get(1);
			
			tab1.add(tab1.size(), tab1.get(0));
			tab1.add(tab1.size(), tab1.get(2));
			tab1.add(tab1.size(), tab1.get(3));
			for(int i =0;i<4;i++)
				tab1.remove(0);
			break;
		case 3 :
			res = tab1.get(2);
			
			tab1.add(tab1.size(), tab1.get(0));
			tab1.add(tab1.size(), tab1.get(1));
			tab1.add(tab1.size(), tab1.get(3));
			for(int i =0;i<4;i++)
				tab1.remove(0);
			break;
		case 4 :
			res = tab1.get(3);
			
			tab1.add(tab1.size(), tab1.get(0));
			tab1.add(tab1.size(), tab1.get(1));
			tab1.add(tab1.size(), tab1.get(2));
			for(int i =0;i<4;i++)
				tab1.remove(0);
			break;
		default :
			break;
		}
		return res;
	}
}
