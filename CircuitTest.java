import static org.junit.Assert.*;

import org.junit.Test;

/**
 * test pour circuit
 */
public class CircuitTest {

	@Test
	public void testConstructeurCircuit() throws TuileException, CircuitException {
		//Pr�paration des donn�es
		Circuit c = new Circuit("Avenue Corsa Paseo");
		//test 
		assertEquals("Le nom du circuit devrait etre Avenue Corsa Paseo",c.getNomCircuit(), "Avenue Corsa Paseo");
	}
	
	@Test
	public void testGetNbCases() throws TuileException, CircuitException {
		//Pr�paration des donn�es
		Circuit c = new Circuit("Avenue Corsa Paseo");
		//test 
		assertEquals("Il devrait y avoir 78 cases", c.getNbCases(),78);
	}
	
	@Test
	public void testPositionEnDescente() throws TuileException, CircuitException {
		//Pr�paration des donn�es
		Circuit c = new Circuit("Avenue Corsa Paseo");
		Cycliste cy = new Cycliste('r',5,1);
		//test 
		boolean test = c.positionEnDescente(cy);
		assertFalse("La position ne devrait pas etre en descente",test);
	}
	
	@Test
	public void testPositionEnMontee() throws TuileException, CircuitException {
		//Pr�paration des donn�es
		Circuit c = new Circuit("Avenue Corsa Paseo");
		Cycliste cy = new Cycliste('r',5,1);
		//test 
		boolean test = c.positionEnMontee(cy);
		assertFalse("La position ne devrait pas etre en montee",test);
	}
	
	@Test
	public void testPositionNormale() throws TuileException, CircuitException {
		//Pr�paration des donn�es
		Circuit c = new Circuit("Avenue Corsa Paseo");
		Cycliste cy = new Cycliste('r',5,1);
		//test 
		boolean test = c.positionNormale(cy);
		assertTrue("La position devrait etre normale",test);
	}
}
