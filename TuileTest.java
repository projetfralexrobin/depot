import static org.junit.Assert.*;

import org.junit.Test;


/**
 * test pour tuile
 */
public class TuileTest {

	@Test
	public void testConstructeurTuile() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",0);
		//test
		assertEquals("Le nom de la tuile aurait du etre a",t.getNomTuile(),'a');
		assertEquals("Le nombre de cases aurait du etre de 6",t.getNombreDeCases(),6);
		assertEquals("Le type aurait du etre LDD",t.getType(),"LDD");
		assertEquals("Il ne devrait pas avoir de pente",t.getPente(),0);
	}
	
	@Test
	public void testConstructeurTuileVirage() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new TuileVirage('a',"LDD",0);
		//test
		assertEquals("Le nom de la tuile aurait du etre a",t.getNomTuile(),'a');
		assertEquals("Le nombre de cases aurait du etre de 2",t.getNombreDeCases(),2);
		assertEquals("Le type aurait du etre LDD",t.getType(),"LDD");
		assertEquals("Il ne devrait pas avoir de pente",t.getPente(),0);
	}
	
	@Test
	public void testConstructeurTuileLigneDroite() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new TuileLigneDroite('a',"LDD",0);
		//test
		assertEquals("Le nom de la tuile aurait du etre a",t.getNomTuile(),'a');
		assertEquals("Le nombre de cases aurait du etre de 6",t.getNombreDeCases(),6);
		assertEquals("Le type aurait du etre LDD",t.getType(),"LDD");
		assertEquals("Il ne devrait pas avoir de pente",t.getPente(),0);
	}
	
	@Test
	public void testEstLigneDroite() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",0);
		//test
		boolean test = t.estLigneDroite();
		assertTrue("La ligne devrait etre droite",test);
	}
	
	@Test
	public void testEstLigneDroite2() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",-1);
		//test
		boolean test = t.estLigneDroite();
		assertFalse("La ligne ne devrait pas etre droite",test);
	}
	
	@Test
	public void testEstMontee() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",1);
		//test
		boolean test = t.estMontee();
		assertTrue("La ligne devrait etre en montee",test);
	}
	
	@Test
	public void testEstMontee2() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",0);
		//test
		boolean test = t.estMontee();
		assertFalse("La ligne ne devrait pas etre en montee",test);
	}
	
	@Test
	public void testEstDescente() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",-1);
		//test
		boolean test = t.estDescente();
		assertTrue("La ligne devrait etre en descente",test);
	}
	
	@Test
	public void testEstDescente2() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",1);
		//test
		boolean test = t.estDescente();
		assertFalse("La ligne ne devrait pas etre en descente",test);
	}
	
	@Test
	public void testSetNomTuile() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",1);
		//test
		t.setNomTuile('b');
		assertEquals("Le nom de la tuile devrait etre b",t.getNomTuile(),'b');
	}
	
	@Test
	public void testSetPente() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",1);
		//test
		t.setPente(-1);
		assertEquals("La pente de la tuile devrait etre a -1",t.getPente(),-1);
	}
	
	@Test
	public void testSetType() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",1);
		//test
		t.setType("LDA");
		assertEquals("Le type de la tuile devrait etre LDA",t.getType(),"LDA");
	}
	
	@Test
	public void testSetNombreDeCases() throws TuileException {
		//Pr�paration des donn�es
		Tuile t = new Tuile('a',6,"LDD",1);
		//test
		t.setNombreDeCases(2);
		assertEquals("Le nombre de case devrait etre a 2",t.getNombreDeCases(),2);
	}
	
	@Test
	public void testEstLeger() throws TuileException {
		//Pr�paration des donn�es
		TuileVirage t = new TuileVirage('a',"LDD",1);
		//test
		boolean test = t.estLeger();
		assertFalse("La tuile ne devrait pas etre virage leger",test);
	}
	
	@Test
	public void testEstLeger2() throws TuileException {
		//Pr�paration des donn�es
		TuileVirage t = new TuileVirage('a',"VLG",1);
		//test
		boolean test = t.estLeger();
		assertTrue("La tuile devrait etre un virage leger",test);
	}
	
	@Test
	public void testEstSerre() throws TuileException {
		//Pr�paration des donn�es
		TuileVirage t = new TuileVirage('a',"VLG",1);
		//test
		boolean test = t.estSerre();
		assertFalse("La tuile ne devrait pas etre un virage serre",test);
	}
	
	@Test
	public void testEstSerre2() throws TuileException {
		//Pr�paration des donn�es
		TuileVirage t = new TuileVirage('a',"VSG",1);
		//test
		boolean test = t.estSerre();
		assertTrue("La tuile devrait etre un virage serre",test);
	}
	
	@Test
	public void testEstLigneDepart() throws TuileException {
		//Pr�paration des donn�es
		TuileLigneDroite t = new TuileLigneDroite('a',"VLG",1);
		//test
		boolean test = t.estLigneDepart();
		assertFalse("La tuile ne devrait pas etre une ligne depart",test);
	}
	
	@Test
	public void testEstLigneDepart2() throws TuileException {
		//Pr�paration des donn�es
		TuileLigneDroite t = new TuileLigneDroite('a',"LDD",1);
		//test
		boolean test = t.estLigneDepart();
		assertTrue("La tuile devrait etre une ligne depart",test);
	}
	
	@Test
	public void testEstLigneNormale() throws TuileException {
		//Pr�paration des donn�es
		TuileLigneDroite t = new TuileLigneDroite('a',"LDA",1);
		//test
		boolean test = t.estLigneNormale();
		assertFalse("La tuile ne devrait pas etre une ligne normale",test);
	}
	
	@Test
	public void testEstLigneNormale2() throws TuileException {
		//Pr�paration des donn�es
		TuileLigneDroite t = new TuileLigneDroite('a',"LDN",1);
		//test
		boolean test = t.estLigneNormale();
		assertTrue("La tuile devrait etre une ligne normale",test);
	}
	
	@Test
	public void testEstLigneArrive() throws TuileException {
		//Pr�paration des donn�es
		TuileLigneDroite t = new TuileLigneDroite('a',"LDD",1);
		//test
		boolean test = t.estLigneArrivee();
		assertFalse("La tuile ne devrait pas etre une ligne arrivee",test);
	}
	
	@Test
	public void testEstLigneArrive2() throws TuileException {
		//Pr�paration des donn�es
		TuileLigneDroite t = new TuileLigneDroite('a',"LDA",1);
		//test
		boolean test = t.estLigneArrivee();
		assertTrue("La tuile devrait etre une ligne arrivee",test);
	}
	
	

}
