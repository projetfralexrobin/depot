/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * class d'exception
 */
public class CircuitException extends Exception{
	/**
	 * Constructeur
	 * @param s, le message envoy�
	 */
	public CircuitException(String s) {
		super(s);
	}
}