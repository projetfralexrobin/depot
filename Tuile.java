/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 * cr�er des tuiles pour plateau
 */
public class Tuile {
	
	private char nomTuile;
	private int pente;
	private int nombreDeCases;
	private String type;
	
	/**
	 * Constructeur
	 * @param nomTuile
	 * @param pente
	 * @param nombreDeCases
	 * @param type
	 * @throws TuileException
	 */

	public Tuile(char c, int nb, String t, int p) throws TuileException {
		this.nomTuile = c;
		this.nombreDeCases = nb;
		if (t.equals("LDN") || t.equals("LDA") || t.equals("LDD") || t.equals("VLG") || t.equals("VLD") || t.equals("VSG") || t.equals("VSD")){
			this.type = t;
		}else {
			throw new TuileException("Erreur dans le type de tuile...");
		}
		if (p == 1 || p == 0 || p == -1) {
			this.pente = p;
		}else {
			throw new TuileException("Erreur dans la valeur de la variable pente...");
		}
	}
	
	/**
	 * Methode estLigneDroite
	 * @return vrai si la tuile est une ligne droite
	 */
	
	public boolean estLigneDroite(){
		boolean res = false;
		if(this.pente == 0) 
			res = true;
		
		return res;
	}
	
	/**
	 * Methode estMontee
	 * @return vrai si la tuile est une mont�e
	 */
	
	public boolean estMontee() {
		boolean res = false;
		if (this.pente == 1) 
			res = true;
			
		return res;
	}
	
	/**
	 * Methode estDescente
	 * @return vrai si la tuile est une pente
	 */
	
	public boolean estDescente() {
		boolean res = false;
		if (this.pente == -1)
			res = true;
		
		return res;
	}

	/**
	 * Methode getNomTuile
	 * @return le nomTuile
	 */
	public char getNomTuile() {
		return nomTuile;
	}

	/**
	 * Methode setNomTuile
	 * @param le nomTuile a set
	 */
	public void setNomTuile(char nomTuile) {
		this.nomTuile = nomTuile;
	}

	/**
	 * Methode getPente
	 * @return la pente
	 */
	public int getPente() {
		return pente;
	}

	/**
	 * Methode setPente
	 * @param la pente a set
	 */
	public void setPente(int pente) {
		this.pente = pente;
	}

	/**
	 * Methode getNombreDeCases
	 * @return le nombreDeCases
	 */
	public int getNombreDeCases() {
		return nombreDeCases;
	}

	/**
	 * Methode setNombreDeCases
	 * @param le nombreDeCases a set 
	 */
	public void setNombreDeCases(int nombreDeCases) {
		this.nombreDeCases = nombreDeCases;
	}

	/**
	 * Methode getType
	 * @return le type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Methode setType
	 * @param le type � set
	 */
	public void setType(String type) {
		this.type = type;
	}
}

