
/**
 * @author prugne2u Prugne Robin
 * @author Alex_ Alexandre Da Silva Carmo
 */

/**
 *class Carte qui creer des cartes avec un numero
 */

public class Carte {
	private int num;
	
	/**
	 * Constructeur
	 * @param n = le numero de la carte
	 */
	public Carte(int n) {
		this.num = n;
	}
	
	/**
	 * Methode getNum
	 * @return num
	 */
	public int getNum() {
		return num;
	}
}
